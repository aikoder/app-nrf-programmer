# app-nrf-programmer

## 1. 工具截图
![screenshot](./screenshot.png)
## 2.如何搭建开发环境
以[annaconda](https://www.anaconda.com/)为例。
- 安装[annaconda](https://www.anaconda.com/)
- 创建开发环境
```
conda create -n pyside6 python=3.9
```

### 如何安装依赖库?
``` 
pip install -r requirements.txt
```

### 如何安装`pynrfjprog`  
源码安装[pynrfjprog](https://github.com/NordicSemiconductor/pynrfjprog.git)
```
python setup.py install
```

## 3.如何发布应用
为了尽量减小应用大小，需要创建一个干净的打包环境:
- #### 安装依赖
```
conda create -n release python=3.9
pip install -r requirements.txt
```
- #### 安装`pynrfjprog`  

### 如何减小生成的执行文件大小？
下载[UPX](https://upx.github.io/)到需要打包工程的根目录下即可，使用`pyinstaller`时会自动调用。  


### 如何打包为`exe`执行文件？
```
pyinstaller -Fw --clean -i title.ico -n nrfFlashTool main.py
```

## 如何生成`requirements.txt`文件?  
```
pipreqs . --encoding=utf8 --force
```